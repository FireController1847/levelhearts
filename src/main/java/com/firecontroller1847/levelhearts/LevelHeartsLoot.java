package com.firecontroller1847.levelhearts;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.entries.LootTableReference;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

import static com.firecontroller1847.levelhearts.LevelHearts.debug;

@EventBusSubscriber(modid = LevelHearts.MOD_ID, bus = Bus.FORGE)
public class LevelHeartsLoot {

    @SubscribeEvent
    public static void onLootTableLoad(LootTableLoadEvent event) {
        if (!Config.enableLoot.get()) {
            return;
        }

        // @formatter:off
        String[] path = event.getName().getPath().split("/");
        if (path[0].equals("chests")) {
            // Load the Rest
            if (path[1].equals("village")) {
                // Intentional Exclusions
                if (
                    path[2].contains("house") ||
                    path[2].equals("village_cartographer") ||
                    path[2].equals("village_fisher") ||
                    path[2].equals("village_fletcher") ||
                    path[2].equals("village_mason") ||
                    path[2].equals("village_shepherd") ||
                    path[2].equals("village_tannery") ||
                    path[2].equals("village_toolsmith")
                ) {
                    return;
                }
                
                event.getTable().addPool(fetchLootPool(String.format("%s/%s/lh_%s", path[0], path[1], path[2])));
            } else {
                // Intentional Exclusions
                if (
                    path[1].equals("igloo_chest") ||
                    path[1].equals("nether_bridge") ||
                    path[1].equals("jungle_temple_dispenser") ||
                    path[1].equals("shipwreck_map") ||
                    path[1].equals("shipwreck_supply") ||
                    path[1].equals("spawn_bonus_chest") ||
                    path[1].equals("stronghold_crossing") ||
                    path[1].equals("stronghold_library") ||
                    path[1].equals("bastion_bridge") ||
                    path[1].equals("bastion_other") ||
                    path[1].equals("bastion_treasure")
                ) {
                    return;
                }
                
                event.getTable().addPool(fetchLootPool(String.format("%s/lh_%s", path[0], path[1])));
            }
        }
        // @formatter:on
    }

    public static LootPool fetchLootPool(String location) {
        return fetchLootPool(LevelHearts.MOD_ID + ":", location);
    }

    public static LootPool fetchLootPool(String namespace, String location) {
        debug("LootTableLoad{Event}: Creating new loot pool @ " + namespace + location);

        LootPoolEntryContainer.Builder<?> entry = LootTableReference.lootTableReference(new ResourceLocation(namespace + location)).setQuality(1);
        return LootPool.lootPool().add(entry).name("levelhearts_inject").build();
    }

}
