package com.firecontroller1847.levelhearts;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.Util;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.Gui.HeartType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.gui.ForgeIngameGui;
import net.minecraftforge.client.gui.IIngameOverlay;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import static net.minecraft.client.gui.GuiComponent.GUI_ICONS_LOCATION;

public class LevelHeartsGui {

    public static final ResourceLocation GUI_HEARTS_LOCATION = new ResourceLocation(LevelHearts.MOD_ID, "textures/gui/hearts.png");

    private Minecraft minecraft;
    private ForgeIngameGui gui;

    public LevelHeartsGui() {
        this.minecraft = Minecraft.getInstance();
        this.gui = (ForgeIngameGui) minecraft.gui;
    }

    @SubscribeEvent
    public void onRenderGameOverlay(RenderGameOverlayEvent.PreLayer event) {
        // Disable all HUD modifications if this option is disabled
        if (!Config.customHud.get()) {
            return;
        }

        IIngameOverlay overlay = event.getOverlay();
        if (Config.minimalHud.get()) {
            /**
             * Minimal HUD requires a full rewrite of the health rendering system.
             */
            if (overlay == ForgeIngameGui.PLAYER_HEALTH_ELEMENT) {
                event.setCanceled(true);
                int width = minecraft.getWindow().getGuiScaledWidth();
                int height = this.minecraft.getWindow().getGuiScaledHeight();
                this.renderMinimalHealth(width, height, event.getMatrixStack());
                gui.left_height += 10;
                RenderGameOverlayEvent eventParent = new RenderGameOverlayEvent(event.getMatrixStack(), event.getPartialTicks(), minecraft.getWindow());
                MinecraftForge.EVENT_BUS.post(new RenderGameOverlayEvent.PostLayer(event.getMatrixStack(), eventParent, overlay));
            }
        } else {
            Player player = (Player) minecraft.getCameraEntity();
            assert player != null;

            /**
             * Instead of redrawing the armor and air ourselves, we increase compatibility
             * by still calling the ForgeGui#renderArmor method, and simply changing the
             * inputs of the function call to move the position of it instead.
             *
             * The width is calculated by offsetting the "width / 2 - 91" by "width + 101 * 2"
             * to give an equivalent value of changing "- 91" to "+ 10".
             */
            if (overlay == ForgeIngameGui.ARMOR_LEVEL_ELEMENT) {
                event.setCanceled(true);
                int width = minecraft.getWindow().getGuiScaledWidth();
                int height = this.minecraft.getWindow().getGuiScaledHeight();
                width += 202;
                gui.left_height = 49;
                overlay.render(gui, event.getMatrixStack(), event.getPartialTicks(), width, height);
                RenderGameOverlayEvent eventParent = new RenderGameOverlayEvent(event.getMatrixStack(), event.getPartialTicks(), minecraft.getWindow());
                MinecraftForge.EVENT_BUS.post(new RenderGameOverlayEvent.PostLayer(event.getMatrixStack(), eventParent, overlay));
            } else if (overlay == ForgeIngameGui.AIR_LEVEL_ELEMENT && player.getArmorValue() > 0) {
                event.setCanceled(true);
                int width = minecraft.getWindow().getGuiScaledWidth();
                int height = this.minecraft.getWindow().getGuiScaledHeight();
                height -= 10;
                overlay.render(gui, event.getMatrixStack(), event.getPartialTicks(), width, height);
                RenderGameOverlayEvent eventParent = new RenderGameOverlayEvent(event.getMatrixStack(), event.getPartialTicks(), minecraft.getWindow());
                MinecraftForge.EVENT_BUS.post(new RenderGameOverlayEvent.PostLayer(event.getMatrixStack(), eventParent, overlay));
            } else if (overlay == ForgeIngameGui.PLAYER_HEALTH_ELEMENT) {
                if (Config.hideHud.get()) {
                    event.setCanceled(true);
                } else if (Config.texturedHud.get() != 0) {
                    event.setCanceled(true);
                    int width = minecraft.getWindow().getGuiScaledWidth();
                    int height = this.minecraft.getWindow().getGuiScaledHeight();
                    this.renderTexturedHealth(width, height, event.getMatrixStack());
                    RenderGameOverlayEvent eventParent = new RenderGameOverlayEvent(event.getMatrixStack(), event.getPartialTicks(), minecraft.getWindow());
                    MinecraftForge.EVENT_BUS.post(new RenderGameOverlayEvent.PostLayer(event.getMatrixStack(), eventParent, overlay));
                }
            }
        }
    }

    /**
     * This function renders the health in a different manner than vanilla
     * Minecraft. However, it largely parallels what is found in
     * ForgeIngameGui#renderHealth. Refer to Forge's rendering
     * method whenever possible.
     */
    private void renderMinimalHealth(int width, int height, PoseStack pStack) {
        //////// FORGE START ////////
        RenderSystem.setShaderTexture(0, GUI_ICONS_LOCATION);
        minecraft.getProfiler().push("health");
        RenderSystem.enableBlend();

        Player player = (Player) minecraft.getCameraEntity();
        assert player != null;
        int health = Mth.ceil(player.getHealth());
        boolean highlight = gui.healthBlinkTime > (long) gui.tickCount && (gui.healthBlinkTime - (long) gui.tickCount) / 3L %2L == 1L;

        if (health < gui.lastHealth && player.invulnerableTime > 0) {
            gui.lastHealthTime = Util.getMillis();
            gui.healthBlinkTime = (long)(gui.tickCount + 20);
        } else if (health > gui.lastHealth && player.invulnerableTime > 0) {
            gui.lastHealthTime = Util.getMillis();
            gui.healthBlinkTime = (long)(gui.tickCount + 10);
        }

        if (Util.getMillis() - gui.lastHealthTime > 1000L) {
            gui.lastHealth = health;
            gui.displayHealth = health;
            gui.lastHealthTime = Util.getMillis();
        }

        gui.lastHealth = health;
        int healthLast = gui.displayHealth;

        AttributeInstance attrMaxHealth = player.getAttribute(Attributes.MAX_HEALTH);
        float healthMax = Math.max((float)attrMaxHealth.getValue(), Math.max(healthLast, health));
        int absorb = Mth.ceil(player.getAbsorptionAmount());

        int healthRows = Mth.ceil((healthMax + absorb) / 2.0F / 10.0F);
        int rowHeight = Math.max(10 - (healthRows - 2), 3);

        gui.random.setSeed((long) (gui.tickCount * 312871));

        //////// FORGE END ////////
        // The following code is an intermix of Forge's system and
        // my new rendering system. Ensure to check compat for
        // each following game version.

        int left = width / 2 - 91;
        int top = height - gui.left_height;

        // Get absorption amount
        int absHealth = Mth.ceil(player.getAbsorptionAmount());
        int absHealthTemp = absHealth;

        // Get amount in full hearts subtract one
        int hearts = Mth.ceil((player.getHealth() + absHealth) / 2.0F) - 1;

        // Get max amount in full hearts subtract one
        int maxHearts = Mth.ceil((player.getMaxHealth() + absHealth) / 2.0F) - 1;

        // Determine how many rows and fetch the top one (-1 for zero index)
        int row = Mth.ceil(((hearts + 1) * 2) / 20.0F) - 1;

        // Determine the maximum amount of rows possible
        int maxRow = Mth.ceil(((maxHearts + 1) * 2) / 20.0F) - 1;

        // Get minimum heart range for row
        int rowMin = row * 10;

        // Get the maximum amount of hearts for this row
        int rowMax = rowMin + 10;

        // Remove extra hearts that would have exceeded the player's maximum heart count on the last row only
        int rowPreferred = row == maxRow ? rowMax - (10 - (maxHearts + 1 - rowMin)) : rowMax;

        for (int i = rowMin; i < rowPreferred; i++) {
            int textureX = 16;
            if (player.hasEffect(MobEffects.POISON)) {
                textureX += 36;
            } else if (player.hasEffect(MobEffects.WITHER)) {
                textureX += 72;
            }

            int regen = -1;
            if (player.hasEffect(MobEffects.REGENERATION)) {
                regen = minecraft.gui.getGuiTicks() % Mth.ceil(player.getMaxHealth() + 5.0F);
            }
            int x = left + (i * 8) - ((8 * 10) * row);
            int y = top;

            // Check if hearts is less than 40% of default health (min 1)
            if (hearts <= Math.max((Config.defHealth.get() / 2.0) * 0.4, 1)) {
                y += gui.random.nextInt(2);
            }

            if (absHealthTemp <= 0 && i == regen) {
                y -= 2;
            }

            // Draw Empty Hearts
            gui.blit(pStack, x, y, 16 + (highlight ? 9 : 0), 0, 9, 9);

            // Draw Highlight Hearts
            if (highlight) {
                if (i * 2 + 1 < gui.displayHealth) {
                    gui.blit(pStack, x, y, textureX + 54, 0, 9, 9);
                }
                if (i * 2 + 1 == gui.displayHealth) {
                    gui.blit(pStack, x, y, textureX + 63, 0, 9, 9);
                }
            }

            // Draw Filled In Hearts
            if (i * 2 + 1 < health) {
                gui.blit(pStack, x, y, textureX + 36, 0, 9, 9);
            } else if (i * 2 + 1 == health) {
                gui.blit(pStack, x, y, textureX + 45, 0, 9, 9);
            } else if (i * 2 + 1 > health && absHealthTemp > 0) {
                if (absHealthTemp == absHealth && absHealth % 2 == 1) {
                    gui.blit(pStack, x, y, textureX + 153, 0, 9, 9);
                    absHealthTemp--;
                } else {
                    gui.blit(pStack, x, y, textureX + 144, 0, 9, 9);
                    absHealthTemp -= 2;
                }
            }
        }

        // Draw MinimalHUD Number
        String text = Integer.toString(row + 1);

        // Move left positioning for more positions in the number
        if (row >= 9)
            left -= 6;
        if (row >= 99)
            left -= 6;
        if (row >= 999)
            left -= 6;
        if (row >= 9999) {
            text = "9999+";
            left -= 6;
        }

        // Dropshadow
        minecraft.font.draw(pStack, text, left - 6, top + 1, 0x000000);
        minecraft.font.draw(pStack, text, left - 8, top + 1, 0x000000);
        minecraft.font.draw(pStack, text, left - 7, top + 2, 0x000000);
        minecraft.font.draw(pStack, text, left - 7, top, 0x000000);

        // The Number
        minecraft.font.draw(pStack, text, left - 7, top + 1, 0xF00000);

        RenderSystem.disableBlend();
        minecraft.getProfiler().pop();
    }

    /**
     * This function renders the health in a similar manner to vanilla
     * Minecraft, but changes the heart's texture using the methods
     * defined in the LevelHearts config. This still largely parallels
     * what is found in ForgeIngameGui#renderHealth, and as such
     * you should still refer to Forge's rendering method whenever
     * possible.
     */
    private void renderTexturedHealth(int width, int height, PoseStack pStack) {
        //////// FORGE START ////////
        RenderSystem.setShaderTexture(0, GUI_ICONS_LOCATION);
        minecraft.getProfiler().push("health");
        RenderSystem.enableBlend();

        Player player = (Player) minecraft.getCameraEntity();
        assert player != null;
        int health = Mth.ceil(player.getHealth());
        boolean highlight = gui.healthBlinkTime > (long) gui.tickCount && (gui.healthBlinkTime - (long) gui.tickCount) / 3L %2L == 1L;

        if (health < gui.lastHealth && player.invulnerableTime > 0) {
            gui.lastHealthTime = Util.getMillis();
            gui.healthBlinkTime = (long)(gui.tickCount + 20);
        } else if (health > gui.lastHealth && player.invulnerableTime > 0) {
            gui.lastHealthTime = Util.getMillis();
            gui.healthBlinkTime = (long)(gui.tickCount + 10);
        }

        if (Util.getMillis() - gui.lastHealthTime > 1000L) {
            gui.lastHealth = health;
            gui.displayHealth = health;
            gui.lastHealthTime = Util.getMillis();
        }

        gui.lastHealth = health;
        int healthLast = gui.displayHealth;

        AttributeInstance attrMaxHealth = player.getAttribute(Attributes.MAX_HEALTH);
        float healthMax = Math.max((float)attrMaxHealth.getValue(), Math.max(healthLast, health));
        int absorb = Mth.ceil(player.getAbsorptionAmount());

        int healthRows = Mth.ceil((healthMax + absorb) / 2.0F / 10.0F);
        int rowHeight = Math.max(10 - (healthRows - 2), 3);

        gui.random.setSeed((long) (gui.tickCount * 312871));

        int left = width / 2 - 91;
        int top = height - gui.left_height;
        gui.left_height += (healthRows * rowHeight);
        if (rowHeight != 10) gui.left_height += 10 - rowHeight;

        int regen = -1;
        if (player.hasEffect(MobEffects.REGENERATION)) {
            regen = gui.tickCount % Mth.ceil(healthMax + 5.0F);
        }
        //////// FORGE END ////////

        this.renderTexturedHearts(pStack, player, left, top, rowHeight, regen, healthMax, health, absorb, highlight, healthRows);

        //////// FORGE START ////////
        RenderSystem.disableBlend();
        minecraft.getProfiler().pop();
        //////// FORGE END ////////
    }

    /**
     * A custom implementation of Gui#renderHearts. Performs
     * essentially the same task, but in a different manner.
     */
    private void renderTexturedHearts(PoseStack pStack, Player player, int left, int top, int rowHeight, int regen, float healthMax, int health, int absorb, boolean highlight, int healthRows) {
        HeartType hearttype = HeartType.forPlayer(player);

        // Get some custom values
        int currentRows = Mth.ceil((health + absorb) / 2.0f / 10.0f);
        int heartsMax = Mth.ceil(healthMax / 2.0);
        int heartsAborb = Mth.ceil(absorb / 2.0);
        int defHealth = Config.defHealth.get();
        int texturedHudType = Config.texturedHud.get();

        // Draw
        for (int i = heartsMax + heartsAborb - 1; i >= 0; i--) {
            // Calculate i (current heart) as health
            int iHealth = i * 2;

            // Calculate current row and heart position
            int row = i / 10;
            int heart = i % 10;

            // Calculate horizontal and vertical position
            int x = left + heart * 8;
            int y = top - row * rowHeight;

            // Check if hearts is less than 40% of default health (min 1)
            if (health + absorb <= Math.max((defHealth / 2.0) * 0.4, 1)) {
                y += gui.random.nextInt(2);
            }

            // Regen movement
            if (i <= heartsMax && i == regen) {
                y -= 2;
            }

            // Draw heart containers
            gui.blit(pStack, x, y, HeartType.CONTAINER.getX(false, highlight), 0, 9, 9);

            // Draw absorption hearts
            if (i >= heartsMax) {
                int iHealthAbs = iHealth - (int) heartsMax;
                if (iHealthAbs < absorb) {
                    Gui.HeartType type = hearttype == HeartType.WITHERED ? hearttype : HeartType.ABSORBING;
                    gui.blit(pStack, x, y, type.getX(iHealthAbs + 1 == absorb, false), 0, 9, 9);
                }
            }

            // Draw hearts lost in damage
            if (highlight && iHealth < gui.displayHealth) {
                boolean halfHeart = iHealth + 1 == gui.displayHealth;
                this.renderTexturedHeart(pStack, healthRows, currentRows, row, halfHeart, true, hearttype, x, y, texturedHudType);
            }

            // Draw hearts
            if (iHealth < health) {
                boolean halfHeart = iHealth + 1 == health;
                this.renderTexturedHeart(pStack, healthRows, currentRows, row, halfHeart, highlight, hearttype, x, y, texturedHudType);
            }
        }
    }

    /**
     * This is a completely custom implementation
     * of Gui#renderHeart.
     */
    private void renderTexturedHeart(PoseStack pStack, int healthRows, int rows, int row, boolean halfHeart, boolean flash, Gui.HeartType hearttype, int x, int y, int coloredHeartsType) {
        if (hearttype != HeartType.NORMAL || coloredHeartsType == 0) {
            gui.blit(pStack, x, y, hearttype.getX(halfHeart, flash), 0, 9, 9);
        } else if (coloredHeartsType == 1) {
            if (row % 12 != 0) {
                RenderSystem.setShaderTexture(0, GUI_HEARTS_LOCATION);
                int offset = ((row - 1) % 12) * 18;
                gui.blit(pStack, x, y, (halfHeart ? 9 + offset : offset), flash ? 9 : 0, 9, 9);
                RenderSystem.setShaderTexture(0, ForgeIngameGui.GUI_ICONS_LOCATION);
            } else {
                gui.blit(pStack, x, y, hearttype.getX(halfHeart, flash), 0, 9, 9);
            }
        } else if (coloredHeartsType == 2) {
            if (healthRows > 1) {
                RenderSystem.setShaderTexture(0, GUI_HEARTS_LOCATION);
                int offset = Math.min(((rows - 2) - (rows - healthRows)) * 18, 10 * 18);
                gui.blit(pStack, x, y, (halfHeart ? 9 + offset : offset), flash ? 9 : 0, 9, 9);
                RenderSystem.setShaderTexture(0, ForgeIngameGui.GUI_ICONS_LOCATION);
            } else {
                gui.blit(pStack, x, y, hearttype.getX(halfHeart, flash), 0, 9, 9);
            }
        } else if (coloredHeartsType == 3) {
            if (rows > 1) {
                RenderSystem.setShaderTexture(0, GUI_HEARTS_LOCATION);
                int offset = Math.min((rows - 2) * 18, 10 * 18);
                gui.blit(pStack, x, y, (halfHeart ? 9 + offset : offset), flash ? 9 : 0, 9, 9);
                RenderSystem.setShaderTexture(0, ForgeIngameGui.GUI_ICONS_LOCATION);
            } else {
                gui.blit(pStack, x, y, hearttype.getX(halfHeart, flash), 0, 9, 9);
            }
        }
    }

}
