package com.firecontroller1847.levelhearts.items;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;

public class ItemHeartPiece extends Item {

    public ItemHeartPiece() {
        super(new Properties().stacksTo(4).tab(CreativeModeTab.TAB_MISC));
    }

}
