package com.firecontroller1847.levelhearts.commands;

import com.firecontroller1847.levelhearts.LevelHearts;
import com.firecontroller1847.levelhearts.capabilities.IMoreHealth;
import com.firecontroller1847.levelhearts.capabilities.MoreHealth;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.tree.LiteralCommandNode;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.EntityArgument;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.ai.attributes.Attributes;

public class LevelHeartsCommand {

    public static void register(CommandDispatcher<CommandSourceStack> dispatcher) {
        LiteralCommandNode<CommandSourceStack> command = dispatcher.register(Commands.literal("levelhearts")
            .requires(context -> context.hasPermission(2))
            .then(Commands.literal("hearts")
                .then(Commands.literal("add").then(Commands.argument("targets", EntityArgument.player()).then(Commands.argument("amount", IntegerArgumentType.integer()).executes(context -> {
                    return addHealth(context.getSource(), EntityArgument.getPlayer(context, "targets"), IntegerArgumentType.getInteger(context, "amount"), true);
                }).then(Commands.argument("heal", BoolArgumentType.bool()).executes(context -> {
                    return addHealth(context.getSource(), EntityArgument.getPlayer(context, "targets"), IntegerArgumentType.getInteger(context, "amount"), BoolArgumentType.getBool(context, "heal"));
                })))))
                .then(Commands.literal("set").then(Commands.argument("targets", EntityArgument.player()).then(Commands.argument("amount", IntegerArgumentType.integer(1)).executes(context -> {
                    return setHealth(context.getSource(), EntityArgument.getPlayer(context, "targets"), IntegerArgumentType.getInteger(context, "amount"), true);
                }).then(Commands.argument("heal", BoolArgumentType.bool()).executes(context -> {
                    return setHealth(context.getSource(), EntityArgument.getPlayer(context, "targets"), IntegerArgumentType.getInteger(context, "amount"), BoolArgumentType.getBool(context, "heal"));
                }))))))
            .then(Commands.literal("containers")
                .then(Commands.literal("add").then(Commands.argument("targets", EntityArgument.player()).then(Commands.argument("amount", IntegerArgumentType.integer(-127, 127)).executes(context -> {
                    return addContainers(context.getSource(), EntityArgument.getPlayer(context, "targets"), IntegerArgumentType.getInteger(context, "amount"), true);
                }).then(Commands.argument("heal", BoolArgumentType.bool()).executes(context -> {
                    return addContainers(context.getSource(), EntityArgument.getPlayer(context, "targets"), IntegerArgumentType.getInteger(context, "amount"), BoolArgumentType.getBool(context, "heal"));
                })))))
                .then(Commands.literal("set").then(Commands.argument("targets", EntityArgument.player()).then(Commands.argument("amount", IntegerArgumentType.integer(0, 127)).executes(context -> {
                    return setContainers(context.getSource(), EntityArgument.getPlayer(context, "targets"), IntegerArgumentType.getInteger(context, "amount"), true);
                }).then(Commands.argument("heal", BoolArgumentType.bool()).executes(context -> {
                    return setContainers(context.getSource(), EntityArgument.getPlayer(context, "targets"), IntegerArgumentType.getInteger(context, "amount"), BoolArgumentType.getBool(context, "heal"));
                }))))))
        );
    }

    private static int addHealth(CommandSourceStack source, ServerPlayer targetPlayer, int amount, boolean heal) {
        LevelHearts.debug("LevelHeartsCommand#addHealth: Adding " + (amount * 2) + " health!");

        // Update Capability Modifier
        IMoreHealth cap = MoreHealth.getFromPlayer(targetPlayer);
        float newModifier = cap.getModifier() + (amount * 2);
        if (newModifier <= -((float) Attributes.MAX_HEALTH.getDefaultValue())) {
            if (amount > 0) {
                newModifier = (float) 1024.0; // Max value of MAX_HEALTH (?)
            } else {
                newModifier = -((float) Attributes.MAX_HEALTH.getDefaultValue()) + 2;
            }
        }
        cap.setModifier(newModifier);
        cap.setRampPosition((short) 0);
        MoreHealth.updateClient(targetPlayer, cap);

        // Apply Modifier
        LevelHearts.applyHealthModifier(targetPlayer, cap.getTrueModifier());
        if (heal) {
            targetPlayer.setHealth(targetPlayer.getMaxHealth());
        } else {
            targetPlayer.setHealth(targetPlayer.getHealth() - 1);
            targetPlayer.setHealth(targetPlayer.getHealth() + 1);
        }
        return 1;
    }

    private static int setHealth(CommandSourceStack source, ServerPlayer targetPlayer, int amount, boolean heal) {
        LevelHearts.debug("LevelHeartsCommand#addHealth: Setting health to " + (amount * 2) + "!");

        // Update Capability Modifier
        IMoreHealth cap = MoreHealth.getFromPlayer(targetPlayer);
        cap.setModifier((amount * 2) - (float) Attributes.MAX_HEALTH.getDefaultValue());
        cap.setRampPosition((short) 0);
        MoreHealth.updateClient(targetPlayer, cap);

        // Apply Modifier
        LevelHearts.applyHealthModifier(targetPlayer, cap.getTrueModifier());
        if (heal) {
            targetPlayer.setHealth(targetPlayer.getMaxHealth());
        } else {
            targetPlayer.setHealth(targetPlayer.getHealth() - 1);
            targetPlayer.setHealth(targetPlayer.getHealth() + 1);
        }
        return 1;
    }

    private static int addContainers(CommandSourceStack source, ServerPlayer targetPlayer, int amount, boolean heal) {
        LevelHearts.debug("LevelHeartsCommand#addHealth: Adding " + amount + " heart containers!");

        // Update Capability Modifier
        IMoreHealth cap = MoreHealth.getFromPlayer(targetPlayer);
        byte newContainers = (byte) (cap.getHeartContainers() + amount);
        if (newContainers < 0) {
            if (amount > 0) {
                newContainers = Byte.MAX_VALUE;
            } else {
                newContainers = 0;
            }
        }
        cap.setHeartContainers(newContainers);
        MoreHealth.updateClient(targetPlayer, cap);

        // Apply Modifier
        LevelHearts.applyHealthModifier(targetPlayer, cap.getTrueModifier());
        if (heal) {
            targetPlayer.setHealth(targetPlayer.getMaxHealth());
        } else {
            targetPlayer.setHealth(targetPlayer.getHealth() - 1);
            targetPlayer.setHealth(targetPlayer.getHealth() + 1);
        }
        return 1;
    }

    private static int setContainers(CommandSourceStack source, ServerPlayer targetPlayer, int amount, boolean heal) {
        LevelHearts.debug("LevelHeartsCommand#addHealth: Setting heart containers to " + amount + "!");

        // Update Capability Modifier
        IMoreHealth cap = MoreHealth.getFromPlayer(targetPlayer);
        cap.setHeartContainers((byte) amount);
        MoreHealth.updateClient(targetPlayer, cap);

        // Apply Modifier
        LevelHearts.applyHealthModifier(targetPlayer, cap.getTrueModifier());
        if (heal) {
            targetPlayer.setHealth(targetPlayer.getMaxHealth());
        } else {
            targetPlayer.setHealth(targetPlayer.getHealth() - 1);
            targetPlayer.setHealth(targetPlayer.getHealth() + 1);
        }
        return 1;
    }

}
